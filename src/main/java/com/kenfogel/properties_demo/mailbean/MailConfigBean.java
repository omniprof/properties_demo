package com.kenfogel.properties_demo.mailbean;

/**
 * Class to contain the information for an email account. This is sufficient for
 * this project but will need more fields if you wish the program to work with
 * mail systems other than GMail. This should be stored in properties file. If
 * you are feeling adventurous you can look into how you might encrypt the
 * password as it will be in a simple text file.
 *
 * @author Ken Fogel
 *
 */
public class MailConfigBean {

    private String userName;
    private String userEmailAddress;
    private String password;

    /**
     * Default Constructor
     */
    public MailConfigBean() {
        this("", "", "");
    }

    /**
     * @param userEmailAddress
     * @param userName
     * @param password
     */
    public MailConfigBean(final String userName, final String userEmailAddress, final String password) {
        super();
        this.userName = userName;
        this.userEmailAddress = userEmailAddress;
        this.password = password;
    }

    /**
     * @return the userName
     */
    public final String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public final void setUserName(final String userName) {
        this.userName = userName;
    }

    /**
     * @return the userName
     */
    public final String getUserEmailAddress() {
        return userEmailAddress;
    }

    /**
     * 
     * @param userEmailAddress
     */
    public final void setUserEmailAddress(final String userEmailAddress) {
        this.userEmailAddress = userEmailAddress;
    }

    /**
     * @return the password
     */
    public final String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public final void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((userName == null) ? 0 : userName.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((userEmailAddress == null) ? 0 : userEmailAddress.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MailConfigBean other = (MailConfigBean) obj;
        if (userName == null) {
            if (other.userName != null) {
                return false;
            }
        } else if (!userName.equals(other.userName)) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (userEmailAddress == null) {
            if (other.userEmailAddress != null) {
                return false;
            }
        } else if (!userEmailAddress.equals(other.userEmailAddress)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MailConfigBean{" + "userName=" + userName + ", userEmailAddress=" + userEmailAddress + ", password=" + password + '}';
    }
    
}
