package com.kenfogel.properties_demo.manager;

import com.kenfogel.properties_demo.manager.PropertiesManager;
import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.kenfogel.properties_demo.mailbean.MailConfigBean;

/**
 * @author Ken
 *
 */
public class TestPropertiesDemo {

    // A Rule is implemented as a class with methods that are associated
    // with the lifecycle of a unit test. These methods run when required.
    // Avoids the need to cut and paste code into every test method.
    @Rule
    public MethodLogger methodLogger = new MethodLogger();

    private PropertiesManager pm;
    private MailConfigBean mailConfig1;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        pm = new PropertiesManager();
        mailConfig1 = new MailConfigBean("Ken Fogel", "cst.send@moose.com", "MooseForever");
    }

    @Test
    public void testWriteText() throws FileNotFoundException, IOException {
        pm.writeTextProperties("", "TextProps", mailConfig1);

        MailConfigBean mailConfig2 = pm.loadTextProperties("", "TextProps");

        assertEquals("The two beans do not match", mailConfig1, mailConfig2);
    }

    @Test
    public void testWriteXml() throws FileNotFoundException, IOException {
        pm.writeXmlProperties("", "TextProps", mailConfig1);

        MailConfigBean mailConfig2 = pm.loadTextProperties("", "TextProps");

        assertEquals("The two beans do not match", mailConfig1, mailConfig2);
    }

    @Test
    public void testReadJarText() throws NullPointerException, IOException {

        MailConfigBean mailConfig2 = pm.loadJarTextProperties("jar.properties");

        assertEquals("The two beans do not match", mailConfig1, mailConfig2);
    }
}
